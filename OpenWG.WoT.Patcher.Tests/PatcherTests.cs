﻿using System;
using System.IO;
using System.Linq;
using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class PatcherTests
    {

        [Fact]
        public void Patcher_1()
        {
            var p1 = new Patcher("assets/client_folder", "assets/updates_folder_mock");
            Assert.True(p1.IsValid);
            
            var p2 = new Patcher("assets/client_folder1", "assets/updates_folder_mock");
            Assert.False(p2.IsValid);

            var p3 = new Patcher("assets/client_folder", "assets/updates_folder_mock1");
            Assert.False(p3.IsValid);
        }
        
        [Fact]
        public void Patcher_2()
        {
            var clientDir = "assets/client_folder";
            var tempDir = "assets/client_folder_patcher2";
            var updateDir = "assets/updates_folder_real";
            var gameInfoFile = Path.Combine(tempDir, "game_info.xml");
            
            IoHelper.DirectoryCopy(clientDir, tempDir, true);

            var p = new Patcher(tempDir, updateDir);
            Assert.True(p.IsValid);
            
            Assert.True(p.Patch());
            
            var clientFileDelete = Path.Combine(tempDir, "res/file_delete.txt");
            var clientFileRdiff = Path.Combine(tempDir, "res/file_rdiff.txt");
            var clientFileXdelta = Path.Combine(tempDir, "res/file_xdelta.txt");
            var clientFileReplace = Path.Combine(tempDir, "res/file_replace.txt");
            var clientFileNew1 = Path.Combine(tempDir, "res/file_new_1.txt");
            var clientFileNew2 = Path.Combine(tempDir, "res/file_new_2.txt");
            var clientFileNew3 = Path.Combine(tempDir, "res/file_new_3.txt");
            Assert.False(File.Exists(clientFileDelete));
            Assert.Equal("1.0.0.1-rdiff-patched", File.ReadAllText(clientFileRdiff));
            Assert.Equal("1.0.0.1-xdelta-patched", File.ReadAllText(clientFileXdelta));
            Assert.Equal("1.0.0.1-replace-patched", File.ReadAllText(clientFileReplace));
            Assert.Equal("1.0.0.1-new", File.ReadAllText(clientFileNew1));
            Assert.Equal("1.0.0.2-new", File.ReadAllText(clientFileNew2));
            Assert.Equal("1.0.0.4-new", File.ReadAllText(clientFileNew3));

            var gi = new GameInfo(gameInfoFile);
            Assert.Equal(new Version("1.0.0.2"), gi.PartsVersion.Single(x => x.Name == "client").InstalledVersion);
            Assert.Equal(new Version("1.0.0.1"), gi.PartsVersion.Single(x => x.Name == "sdcontent").InstalledVersion);
            Assert.Equal(new Version("1.0.0.4"), gi.PartsVersion.Single(x => x.Name == "hdcontent").InstalledVersion); 

            
            Directory.Delete(tempDir, true);
        }
    }
}