﻿using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class Adler32Tests
    {
        [Fact]
        public void Adler32_1()
        {
            var a = new Adler32();

            var s = new byte[4];
            s[0] = (byte) 'T';
            s[1] = (byte) 'E';
            s[2] = (byte) 'S';
            s[3] = (byte) 'T';
;
            a.Update(s, s.Length);
            
            Assert.Equal("31D0141", a.ResultString());
        }
        
        [Fact]
        public void Adler32_2()
        {
            var a = new Adler32();
            
            a.UpdateFile("assets/garbage/splashscreen.png");
            Assert.Equal("5B02A7BF",a.ResultString());
        }
    }
}