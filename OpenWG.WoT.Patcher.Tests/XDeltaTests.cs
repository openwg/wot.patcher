﻿using System.IO;
using System.Net;
using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class XDeltaTests
    {
        [Fact]
        public void XDelta_1()
        {
            var originalFile = "assets/xdelta/original.txt";
            var outFile = "assets/xdelta/original_patched_1.txt";
            var patchFile = "assets/xdelta/patch.xdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }

            Assert.True(XDelta.PatchFile(originalFile, patchFile, outFile));
        }
        
        [Fact]
        public void XDelta_2()
        {
            var originalFile = "assets/xdelta/original.txt";
            var originalFileCopy = "assets/xdelta/original_copy.txt";
            var patchFile = "assets/xdelta/patch.xdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(originalFileCopy))
            {
                File.Delete(originalFileCopy);
            }
            File.Copy(originalFile, originalFileCopy);

            Assert.True(XDelta.PatchFile(originalFileCopy, patchFile, originalFileCopy));
        }
        
        [Fact]
        public void XDelta_3()
        {
            var originalFile = "assets/xdelta/original.txt";
            var outFile = "assets/xdelta/original_patched_3.txt";
            var patchFile = "assets/xdelta/patch.xdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }
            
            Assert.Equal("1.0.0.0-xdelta-original", File.ReadAllText(originalFile));

            Assert.True(XDelta.PatchFile(originalFile, patchFile, outFile));
            
            Assert.Equal("1.0.0.1-xdelta-patched", File.ReadAllText(outFile));
        }
    }
}