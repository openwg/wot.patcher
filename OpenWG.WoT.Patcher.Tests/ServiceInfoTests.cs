﻿using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class ServiceInfoTests
    {
        [Fact]
        public void ServiceInfo_1()
        {
            var si = new ServiceInfo("assets/service_file/service.xml");
            Assert.Equal(8, si.FilesToDelete.Count);
            Assert.Equal(18, si.FilesToApplyDiff.Count);
        }

        [Fact]
        public void ServiceInfo_2()
        {
            var si = new ServiceInfo("assets/service_not_existing.xml");
            Assert.Empty(si.FilesToDelete);
            Assert.Empty(si.FilesToApplyDiff);
        }

        [Fact]
        public void ServiceInfo_3()
        {
            var si = new ServiceInfo("assets/client_folder/res/engine_config.xml");
            Assert.Empty(si.FilesToDelete);
            Assert.Empty(si.FilesToApplyDiff);
        }

        [Fact]
        public void ServiceInfo_4()
        {
            var si = new ServiceInfo("assets/garbage/splashscreen.png");
            Assert.Empty(si.FilesToDelete);
            Assert.Empty(si.FilesToApplyDiff);
        }
    }
}
