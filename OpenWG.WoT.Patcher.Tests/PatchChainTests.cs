﻿using System;
using System.Linq;

using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class PatchChainTests
    {
        [Fact]
        public void PatchChain_1()
        {
            var pc = new PatchChain();
            Assert.Empty(pc.Parts);

            pc.AddFile("wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.Single(pc.Parts);
            
            
            pc.AddFile("wot_1.11.0.20872_1.10.1.20850_sdcontent.wgpkg");
            pc.AddFile("wot_1.11.0.20872_1.10.1.20850_client.wgpkg");
            Assert.Equal(3, pc.Parts.Count);
        }
        
        [Fact]
        public void PatchChain_2()
        {
            var pc = new PatchChain();
            Assert.Empty(pc.Parts);

            pc.AddFile("wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.Single(pc.Parts);
            
            
            pc.AddFile("wot_1.11.0.20872_1.10.1.20850_sdcontent.other");
            Assert.Single(pc.Parts);
        }


        [Fact]
        public void PatchChain_3()
        {
            var updatesFolder = "assets/updates_folder_mock";

            var pc = PatchChain.FromDirectory(updatesFolder, false);
            Assert.Equal(2, pc.Parts.Count);

            var pc2 = PatchChain.FromDirectory(updatesFolder, true);
            Assert.Equal(3, pc2.Parts.Count);
        }
    }
}
