﻿using System.IO;
using System.Net;
using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class RDiffTests
    {
        [Fact]
        public void RDiff_1()
        {
            var originalFile = "assets/rdiff/original.txt";
            var outFile = "assets/rdiff/original_patched_1.txt";
            var patchFile = "assets/rdiff/patch.rdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }

            Assert.True(RDiff.PatchFile(originalFile, patchFile, outFile));
        }
        
        [Fact]
        public void RDiff_2()
        {
            var originalFile = "assets/rdiff/original.txt";
            var originalFileCopy = "assets/rdiff/original_copy.txt";
            var patchFile = "assets/rdiff/patch.rdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(originalFileCopy))
            {
                File.Delete(originalFileCopy);
            }
            File.Copy(originalFile, originalFileCopy);

            Assert.True(RDiff.PatchFile(originalFileCopy, patchFile, originalFileCopy));
        }
        
        [Fact]
        public void RDiff_3()
        {
            var originalFile = "assets/rdiff/original.txt";
            var outFile = "assets/rdiff/original_patched_3.txt";
            var patchFile = "assets/rdiff/patch.rdiff";

            Assert.True(File.Exists(originalFile));
            Assert.True(File.Exists(patchFile));
            
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }
            
            Assert.Equal("1.0.0.0-rdiff-original", File.ReadAllText(originalFile));

            Assert.True(RDiff.PatchFile(originalFile, patchFile, outFile));
            
            Assert.Equal("1.0.0.1-rdiff-patched", File.ReadAllText(outFile));
        }
    }
}