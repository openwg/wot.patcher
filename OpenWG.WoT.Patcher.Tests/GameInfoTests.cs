﻿using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class GameInfoTests
    {
        [Fact]
        public void GameInfo_1()
        {
            var gi = new GameInfo("assets/client_folder/game_info.xml");
            Assert.True(gi.IsValid);
            Assert.Equal("WOT.RU.PRODUCTION", gi.GameId);
            Assert.Equal("hd", gi.ClientType);
            Assert.Equal("1.0.0.0", gi.Version);
            Assert.Equal(4, gi.PartsVersion.Count);
        }
        
        [Fact]
        public void GameInfo_2()
        {
            var gi = new GameInfo("assets/game_info_not_exists.xml");
            Assert.False(gi.IsValid);

            var gi2 = new GameInfo("assets/splashscreen.bmp");
            Assert.False(gi2.IsValid);
        }
    }
}