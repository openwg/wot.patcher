﻿using System.IO;

using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class IoHelperTests
    {
        [Fact]
        public void IoHelper_1()
        {
            var from = "assets/client_folder";
            var to = "assets/client_folder_iohelper_1";

            Assert.True(Directory.Exists(from));

            if (Directory.Exists(to))
            {
                Directory.Delete(to,true);
            }
            Assert.False(Directory.Exists(to));
            
            IoHelper.DirectoryCopy(from, to, true);
            Assert.True(Directory.Exists(to));
            
            Directory.Delete(to, true);
        }
    }
}