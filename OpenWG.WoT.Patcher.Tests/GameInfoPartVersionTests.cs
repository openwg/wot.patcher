﻿using System;

using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class GameInfoPartVersionTests
    {
        [Fact]
        public void GameInfoPartVersion_1()
        {
            var pv = new GameInfoPartVersion(
                @"<version available=""1.10.1.20851"" name=""client"" installed=""1.10.1.20850""/>");
            
            Assert.True(pv.IsValid);
            Assert.Equal(new Version("1.10.1.20851"), pv.AvailableVersion);
            Assert.Equal(new Version("1.10.1.20850"), pv.InstalledVersion);
            Assert.Equal("client", pv.Name);
        }
        
        [Fact]
        public void GameInfoPartVersion_2()
        {
            var pv = new GameInfoPartVersion(
                @"aslsk");
            
            Assert.False(pv.IsValid);
        }
    }
}