﻿using System;
using System.IO;
using Xunit;

namespace OpenWG.WoT.Patcher.Tests
{
    public class PatchPartTests
    {
        private void copyClient(string from, string to)
        {
            if (Directory.Exists(to))
            {
                Directory.Delete(to,true);
            }
            Assert.False(Directory.Exists(to));

            IoHelper.DirectoryCopy(from, to, true);
            Assert.True(Directory.Exists(to));
        }
        
        [Fact]
        public void PatchPart_Name_1()
        {
            var pp = new PatchPart("wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.Equal("wot", pp.ApplicationTag);
            Assert.Equal("hdcontent", pp.ApplicationComponent);
            Assert.Equal(new Version("1.10.1.20850"), pp.VersionFrom);
            Assert.Equal(new Version("1.11.0.20872"), pp.VersionTo);
            Assert.Equal("wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg", pp.Filepath);
            Assert.True(pp.IsValid);
        }
        
        [Fact]
        public void PatchPart_Name_2()
        {
            var pp = new PatchPart("wot_1.11.0.1471868_locale_ru.wgpkg");
            Assert.Equal("wot", pp.ApplicationTag);
            Assert.Equal("locale_ru", pp.ApplicationComponent);
            Assert.Equal(new Version("0.0.0.0"), pp.VersionFrom);
            Assert.Equal(new Version("1.11.0.1471868"), pp.VersionTo);
            Assert.True(pp.IsValid);
        }
            
        [Fact]
        public void PatchPart_Name_3() {
            var pp = new PatchPart("wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.True(pp.IsValid);

            var pp2 = new PatchPart("D:\\wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.True(pp2.IsValid);

            var pp3 = new PatchPart("D:/wot_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.True(pp2.IsValid);
        }

        [Fact]
        public void PatchPart_Name_4()
        {
            var pp = new PatchPart("_1.11.0.20872_1.10.1.20850_hdcontent.wgpkg");
            Assert.False(pp.IsValid);
            Assert.Empty(pp.Filepath);

            var pp3 = new PatchPart("random_garbage.txt");
            Assert.False(pp3.IsValid);
            Assert.Empty(pp3.Filepath);

            var pp4 = new PatchPart("wot_1.11.0.H20872_1.10.1A.20850_hdcontent.wgpkg");
            Assert.False(pp4.IsValid);
            Assert.Empty(pp4.Filepath);
        }

        [Fact]
        public void PatchPart_Patching_1()
        {
            var clientDir = "assets/client_folder";
            var tempDir = "assets/client_folder_patchpart4";
            var partFile = "assets/updates_folder_real/wot_1.0.0.1_1.0.0.0_client.wgpkg";

            var clientFileDelete = Path.Combine(tempDir, "res/file_delete.txt");
            var clientFileRdiff = Path.Combine(tempDir, "res/file_rdiff.txt");
            var clientFileXdelta = Path.Combine(tempDir, "res/file_xdelta.txt");
            var clientFileReplace = Path.Combine(tempDir, "res/file_replace.txt");
            copyClient(clientDir, tempDir);

            var pp = new PatchPart(partFile);
            
            Assert.Equal("1.0.0.0-delete-original", File.ReadAllText(clientFileDelete));
            Assert.Equal("1.0.0.0-rdiff-original", File.ReadAllText(clientFileRdiff));
            Assert.Equal("1.0.0.0-xdelta-original", File.ReadAllText(clientFileXdelta));
            Assert.Equal("1.0.0.0-replace-original", File.ReadAllText(clientFileReplace));

            Assert.True(pp.Apply(tempDir));

            Assert.False(File.Exists(clientFileDelete));
            Assert.Equal("1.0.0.1-rdiff-patched", File.ReadAllText(clientFileRdiff));
            Assert.Equal("1.0.0.1-xdelta-patched", File.ReadAllText(clientFileXdelta));
            Assert.Equal("1.0.0.1-replace-patched", File.ReadAllText(clientFileReplace));

            Directory.Delete(tempDir, true);
        }
        
        [Fact]
        public void PatchPart_Patching_2()
        {
            var clientDir = "assets/client_folder";
            var tempDir = "assets/client_folder_patchpart5";
            var partFile = "assets/updates_folder_real/wot_1.0.0.1_1.0.0.0_invalid.wgpkg";

            copyClient(clientDir, tempDir);

            var pp = new PatchPart(partFile);
            Assert.False(pp.Apply(tempDir));
            
            Directory.Delete(tempDir, true);
        }
    }
}
