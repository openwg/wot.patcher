﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace OpenWG.WoT.Patcher
{
    public class ServiceInfo
    {
        #region Properties

        public List<string> FilesToDelete { get; } = new List<string>();

        /// <summary>
        /// List of KVPs files which should be patched
        /// 
        /// pair key - file path relative to game root
        /// pair value - result file size
        /// </summary>
        public List<KeyValuePair<string, long>> FilesToApplyDiff { get; } = new List<KeyValuePair<string, long>>();

        #endregion

        public ServiceInfo()
        {

        }

        public ServiceInfo(string filepath)
        {
            Load(filepath);
        }

        public void Load(string filepath)
        {
            FilesToApplyDiff.Clear();
            FilesToDelete.Clear();

            if (!File.Exists(filepath))
            {
                return;
            }

            try
            {
                var xdoc = XDocument.Load(filepath);

                var patchServiceInfo = xdoc.Root.Element("patch_service_info");
                if (patchServiceInfo == null)
                {
                    return;
                }

                //files to delete
                var filesToDelete = patchServiceInfo.Element("files_to_delete")?.Descendants("file")?.Select(x => x.Value);
                if (filesToDelete != null)
                {
                    foreach (var el in filesToDelete)
                    {
                        FilesToDelete.Add(el);
                    }
                }

                //files to apply diff
                var filesToApply = patchServiceInfo.Element("files_to_apply_diff")?.Descendants("file");
                if (filesToApply != null)
                {
                    foreach (var el in filesToApply)
                    {
                        FilesToApplyDiff.Add(new KeyValuePair<string, long>(el.Value, Convert.ToInt64(el.Attribute("result_size").Value)));
                    }
                }
            }
            catch (System.Xml.XmlException)
            {

            }
        }
    }
}
