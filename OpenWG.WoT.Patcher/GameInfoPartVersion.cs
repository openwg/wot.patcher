﻿using System;
using System.Xml;
using System.Xml.Linq;

namespace OpenWG.WoT.Patcher
{
    public class GameInfoPartVersion
    {
        public string Name => _element.Attribute("name")?.Value;

        public Version AvailableVersion => new Version(_element.Attribute("available")?.Value ?? "0.0.0.0");

        public Version InstalledVersion
        {
            get
            {
                return new Version(_element.Attribute("installed")?.Value ?? "0.0.0.0");
            }
            set
            {
                var attr = _element.Attribute("installed");
                if (attr == null)
                {
                    attr = new XAttribute("installed", "");
                    _element.Add(attr);
                }
                attr.Value = value.ToString();
            }
        }

        public bool IsValid => _element != null;

        private XElement _element { get; } = null;

        public GameInfoPartVersion(XElement el)
        {
            _element = el;
        }

        public GameInfoPartVersion(string xmlString)
        {
            try
            {
                _element = XElement.Parse(xmlString);
            }
            catch (XmlException) { }
        }
    }
}
