﻿using System.Diagnostics;
using System.IO;

namespace OpenWG.WoT.Patcher
{
    public static class RDiff
    {
        private static string getRdiffExecutable()
        {
            if (!File.Exists("rdiff.exe"))
            {
                return null;
            }

            return Path.GetFullPath("rdiff.exe");
        }

        public static bool PatchFile(string oldFile, string diffFile, string outFile)
        {
            if (!File.Exists(oldFile) || !File.Exists(diffFile))
            {
                return false;
            }

            bool moveAfterPatch = false;
            if (outFile == oldFile)
            {
                moveAfterPatch = true;
                outFile += ".temp";
            }

            var rdiffPath = getRdiffExecutable();
            if (string.IsNullOrWhiteSpace(rdiffPath))
            {
                return false;
            }

            var p = new Process();
            p.StartInfo.FileName = rdiffPath;
            p.StartInfo.ArgumentList.Add("patch");
            p.StartInfo.ArgumentList.Add(oldFile);
            p.StartInfo.ArgumentList.Add(diffFile);
            p.StartInfo.ArgumentList.Add(outFile);

            p.Start();
            p.WaitForExit();

            bool isOk = p.ExitCode == 0;
            if (!isOk)
            {
                return false;
            }

            if (moveAfterPatch)
            {
                File.Move(outFile, oldFile, true);
            }

            return true;
        }
    }
}