﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace OpenWG.WoT.Patcher
{
    public class GameInfo
    {

        private XDocument _document { get; } = null;

        private string _filepath { get; } = "";

        public string GameId => _document.Descendants("id").FirstOrDefault()?.Value;
        public string ClientType => _document.Descendants("client_type").FirstOrDefault()?.Value;
        public string Version => _document.Descendants("version_name").FirstOrDefault()?.Value;

        public IList<GameInfoPartVersion> PartsVersion =>
            _document.Descendants("version").Select(x => new GameInfoPartVersion(x)).ToList();

        public bool IsValid => _document != null;

        public GameInfo(string filepath)
        {
            if (!File.Exists(filepath))
            {
                return;
            }

            try
            {
                _document = XDocument.Load(filepath);
            }
            catch (XmlException)
            {

            }

            _filepath = filepath;
        }

        public void Save()
        {
            _document.Save(_filepath);
        }
    }
}
