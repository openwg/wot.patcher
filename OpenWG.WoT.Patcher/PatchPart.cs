﻿using System;
using System.IO;
using System.Linq;

namespace OpenWG.WoT.Patcher
{
    public class PatchPart
    {
        public string ApplicationTag { get; } = "";

        public string ApplicationComponent { get; } = "";

        public Version VersionFrom { get; } = null;

        public Version VersionTo { get; } = null;

        public string Filepath { get; } = "";

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ApplicationTag) || string.IsNullOrWhiteSpace(ApplicationComponent) || VersionFrom == null || VersionTo == null)
                {
                    return false;
                }

                return true;
            }
        }

        public PatchPart(string filepath)
        {
            if (string.IsNullOrWhiteSpace(filepath))
            {
                return;
            }

            if (Path.GetExtension(filepath) != ".wgpkg" && Path.GetExtension(filepath) != ".dspkg")
            {
                return;
            }

            var tokens = Path.GetFileNameWithoutExtension(filepath).Split('_');

            if (tokens.Any(x => string.IsNullOrWhiteSpace(x)))
            {
                return;
            }

            if (tokens.Length < 3)
            {
                return;
            }
            
            //[0] -- application tag
            ApplicationTag = tokens[0];
            
            //[1] -- version to
            try
            {
                VersionTo = new Version(tokens[1]);
            }
            catch (Exception)
            {
                return;
            }
            
            //[2] -- version from or component
            try
            {
                VersionFrom = new Version(tokens[2]);
            }
            catch (Exception)
            {
                ApplicationComponent = tokens[2];
                VersionFrom = new Version("0.0.0.0");
            }

            //[3+] -- application component
            for (int i = 3; i < tokens.Length; i++)
            {
                ApplicationComponent += $"_{tokens[i]}";
            }
            ApplicationComponent = ApplicationComponent.TrimStart('_');

            Filepath = filepath;
        }


        public bool Apply(string gameDirectory, Patcher.LogHandler logHandler = null)
        {
            //check that directory and file exists
            if (!Directory.Exists(gameDirectory) || !File.Exists(Filepath))
            {
                logHandler?.Invoke("    * FAIL: could not find game directory or package file");
                return false;
            }

            //delete service file if exists
            var serviceDirectory = Path.Combine(gameDirectory, "_service/");
            var serviceFilepath = Path.Combine(serviceDirectory, "service.xml");
            if (File.Exists(serviceFilepath))
            {
                File.Delete(serviceFilepath);
            }

            //extract all files with overwrite
            if (!SevenZip.UnpackFile(Filepath, gameDirectory))
            {
                logHandler?.Invoke("    * FAIL: could not unpack package file");
                return false;
            }

            //parse service file if exists
            if (!File.Exists(serviceFilepath))
            {
                return true;
            }
            
            var service = new ServiceInfo(serviceFilepath);

            foreach (var deleteFile in service.FilesToDelete)
            {
                var deleteFilepath = Path.Combine(gameDirectory, deleteFile);
                if (File.Exists(deleteFilepath))
                {
                    File.Delete(deleteFilepath);
                }
            }

            foreach (var fileToPatch in service.FilesToApplyDiff)
            {
                var originalFilepath = Path.Combine(gameDirectory, fileToPatch.Key);
                var originalFilename = Path.GetFileName(originalFilepath);
                var originalDirname = Path.GetDirectoryName(originalFilepath);
                logHandler?.Invoke($"    * Patching {originalFilename}");

                if (!File.Exists(originalFilepath))
                {
                    logHandler?.Invoke($"        * FAIL: {originalFilepath} does not exists");
                    return false;
                }

                var patchFile = "";
                var patchType = "";

                //find patch
                var rdiff_files = Directory.GetFiles(originalDirname, $"{originalFilename}.*.rdiff");
                var wdsfc_files = Directory.GetFiles(originalDirname, $"{originalFilename}.*.wdsfc");
                var xdelta_files = Directory.GetFiles(originalDirname, $"{originalFilename}.*.xdiff");
                if (rdiff_files.Length == 1)
                {
                    patchFile = rdiff_files.First();
                    patchType = "RDIFF";
                }
                else if (wdsfc_files.Length == 1)
                {
                    patchFile = wdsfc_files.First();
                    patchType = "WDSFC";
                }
                else if (xdelta_files.Length == 1)
                {
                    patchFile = xdelta_files.First();
                    patchType = "XDELTA";
                }

                //apply patch
                bool patchResult = false;
                switch (patchType)
                {
                    case "RDIFF":
                        patchResult = RDiff.PatchFile(originalFilepath, patchFile, originalFilepath);
                        File.Delete(patchFile);
                        break;
                    case "WDSFC":
                        File.Move(patchFile, originalFilepath, true);
                        patchResult = true;
                        break;
                    case "XDELTA":
                        patchResult = XDelta.PatchFile(originalFilepath, patchFile, originalFilepath);
                        File.Delete(patchFile);
                        break;
                    default:
                        break;
                }

                //check patch result
                if (!patchResult)
                {
                    logHandler?.Invoke($"        * FAIL: failed to perform {patchType} patching");
                    return false;
                }
                
                //check reuslt file length
                if (new FileInfo(originalFilepath).Length != fileToPatch.Value)
                {
                    logHandler?.Invoke($"        * FAIL: result size is not match");
                    return false;
                }
                
                //check result Adler32 hash
                var adler = new Adler32();
                adler.UpdateFile(originalFilepath);

                if (adler.Result() != getHash(patchFile))
                {
                    logHandler?.Invoke($"        * FAIL: result Adler32 hash is not match");
                    return false;
                }
                
                logHandler?.Invoke($"        * OK");
            }
            
            //delete service.xml after patching
            if (File.Exists(serviceFilepath))
            {
                File.Delete(serviceFilepath);
            }
            if (Directory.Exists(serviceDirectory) && Directory.GetDirectories(serviceDirectory).Length == 0 &&
                Directory.GetFiles(serviceDirectory).Length == 0)
            {
                Directory.Delete(serviceDirectory);
            }
            
            return true;
        }

        private uint getHash(string filepath)
        {
            var result = 0u;

            var filename = Path.GetFileNameWithoutExtension(filepath);
            var tokens = filename.Split('.');

            try
            {
                result = Convert.ToUInt32(tokens.Last(), 16);
            }
            catch(Exception){}
            
            return result;
        }
    }
}
