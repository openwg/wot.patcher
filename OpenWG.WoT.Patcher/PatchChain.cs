﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWG.WoT.Patcher
{
    public class PatchChain
    {
        public List<PatchPart> Parts { get; } = new List<PatchPart>();

        public static PatchChain FromDirectory(string directoryPath, bool recursively = false)
        {
            if (!Directory.Exists(directoryPath))
            {
                return new PatchChain();
            }

            var pc = new PatchChain();
            var t = Directory.EnumerateFiles(directoryPath, "*.*",
                recursively ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                .Where(s => s.EndsWith(".wgpkg") || s.EndsWith(".dspkg")).ToList();
            pc.AddFiles(t);

            return pc;
        }

        public PatchChain()
        {

        }

        public bool AddFiles(IList<string> patchFiles)
        {
            if (patchFiles == null || patchFiles.Count == 0)
            {
                return false;
            }

            var isFine = true;
            foreach (var patchFile in patchFiles)
            {
                if (!AddFile(patchFile))
                {
                    isFine = false;
                }
            }

            return isFine;
        }

        public bool AddFile(string patchFile)
        {
            var patchPart = new PatchPart(patchFile);
            if (!patchPart.IsValid)
            {
                return false;
            }

            Parts.Add(patchPart);

            return true;
        }
    }
}
