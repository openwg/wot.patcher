﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWG.WoT.Patcher
{
    /// <summary>
    /// Info:
    ///   * https://en.wikipedia.org/wiki/Adler-32
    ///   * https://software.intel.com/content/www/us/en/develop/articles/fast-computation-of-adler32-checksums.html
    /// </summary>
    public class Adler32
    {
        private uint _a = 1;
        private uint _b = 0;

        private const int _fileBlockSize = 4 * 1024 * 1024;

        public Adler32()
        {
        }

        public void Clear()
        {
            _a = 1;
            _b = 0;
        }

        public void UpdateFile(string filepath)
        {
            if (!File.Exists(filepath))
            {
                return;
            }

            using var fileStream = File.OpenRead(filepath);
            var buffer = new byte[_fileBlockSize];
            while (fileStream.Position < fileStream.Length)
            {
                var bytesRead = fileStream.Read(buffer);
                Update(buffer, bytesRead);
            }
        }

        public void Update(byte[] bytes, int length)
        {
            int bytesProcessed = 0;

            while (bytesProcessed < length)
            {
                var loopEnd = Math.Min(5552, length-bytesProcessed);

                for (var i = 0; i < loopEnd; i++)
                {
                    _a += bytes[bytesProcessed+i];
                    _b += _a;
                }

                _a %= 65521;
                _b %= 65521;

                bytesProcessed += loopEnd;
            }
        }

        public uint Result()
        {
            return (_b << 16) | _a;
        }

        public string ResultString()
        {
            return Result().ToString("X");
        }
    }
}
