﻿using System.Diagnostics;
using System.IO;

namespace OpenWG.WoT.Patcher
{
    public static class SevenZip
    {
        private static string get7zipExecutable()
        {
            if (!File.Exists("7za.exe"))
            {
                return null;
            }

            return Path.GetFullPath("7za.exe");
        }

        public static bool UnpackFile(string archiveFile, string destinationDir)
        {
            if (!File.Exists(archiveFile) || !Directory.Exists(destinationDir))
            {
                return false;
            }

            var sevenZipPath = get7zipExecutable();
            if (string.IsNullOrWhiteSpace(sevenZipPath))
            {
                return false;
            }

            var p = new Process();
            p.StartInfo.FileName = sevenZipPath;
            p.StartInfo.ArgumentList.Add("x");
            p.StartInfo.ArgumentList.Add("-y");
            p.StartInfo.ArgumentList.Add("-bd");
            p.StartInfo.ArgumentList.Add(archiveFile);
            p.StartInfo.ArgumentList.Add($"-o{destinationDir}");
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            
            p.Start();
            p.WaitForExit();

            return p.ExitCode == 0;
        }
    }
}