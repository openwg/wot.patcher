﻿using System.Diagnostics;
using System.IO;

namespace OpenWG.WoT.Patcher
{
    public static class XDelta
    {
        private static string getXdeltaExecutable()
        {
            if (!File.Exists("xdelta3.exe"))
            {
                return null;
            }

            return Path.GetFullPath("xdelta3");
        }

        public static bool PatchFile(string oldFile, string diffFile, string outFile)
        {
            if (!File.Exists(oldFile) || !File.Exists(diffFile))
            {
                return false;
            }

            bool moveAfterPatch = false;
            if (outFile == oldFile)
            {
                moveAfterPatch = true;
                outFile += ".temp";
            }

            var xdeltaPath = getXdeltaExecutable();
            if (string.IsNullOrWhiteSpace(xdeltaPath))
            {
                return false;
            }

            var p = new Process();
            p.StartInfo.FileName = xdeltaPath;
            p.StartInfo.ArgumentList.Add("-d");
            //p.StartInfo.ArgumentList.Add("-f");
            p.StartInfo.ArgumentList.Add("-s");
            p.StartInfo.ArgumentList.Add(oldFile);
            p.StartInfo.ArgumentList.Add(diffFile);
            p.StartInfo.ArgumentList.Add(outFile);

            p.Start();
            p.WaitForExit();

            bool isOk =  p.ExitCode == 0;
            if (!isOk)
            {
                return false;
            }

            if (moveAfterPatch)
            {
                File.Move(outFile, oldFile, true);
            }

            return true;
        }
    }
}