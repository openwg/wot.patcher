﻿using System;
using System.IO;
using System.Linq;

namespace OpenWG.WoT.Patcher
{
    public class Patcher
    {
        public string GameFolder { get; } = "";

        public string UpdatesFolder { get; } = "";

        public GameInfo GameInfo { get; } = null;

        public PatchChain PatchChain { get; } = null;

        public bool IsValid { get; } = false;
        
        public delegate void LogHandler(string message);
        public event LogHandler Log;
         
        public Patcher(string gameFolder, string updatesFolder)
        {
            GameFolder = gameFolder;
            UpdatesFolder = updatesFolder;

            if (!Directory.Exists(GameFolder) || !Directory.Exists(UpdatesFolder))
            {
                return;
            }

            //game info
            var gameInfoFile = Path.Combine(GameFolder, "game_info.xml");
            if (!File.Exists(gameInfoFile))
            {
                return;
            }

            GameInfo = new GameInfo(gameInfoFile);
            if (!GameInfo.IsValid)
            {
                return;
            }

            PatchChain = PatchChain.FromDirectory(updatesFolder, true);
            if (PatchChain.Parts.Count == 0)
            {
                return;
            }

            IsValid = true;
        }

        public bool Patch()
        {
            var partsVersion = GameInfo.PartsVersion;

            var shouldContinue = true;
            while (shouldContinue)
            {
                shouldContinue = false;

                foreach (var installedPartVer in partsVersion)
                {
                    var partsComponent = PatchChain.Parts.Where(x => x.ApplicationComponent == installedPartVer.Name).ToList();
                    
                    //try to find from-->to patch
                    var targetPart = partsComponent.SingleOrDefault(x => x.VersionFrom == installedPartVer.InstalledVersion);
                    
                    //else try to find any-->to patch
                    if (targetPart == null)
                    {
                        targetPart = partsComponent.Where(x => x.VersionFrom == new Version("0.0.0.0") && x.VersionTo > installedPartVer.InstalledVersion)
                            .OrderByDescending(i => i.VersionTo).FirstOrDefault();
                    }

                    if (targetPart == null)
                    {
                        continue;
                    }

                    Log?.Invoke($"* PATCH: component={targetPart.ApplicationComponent}, from={targetPart.VersionFrom}, to={targetPart.VersionTo}");
                    
                    if (!targetPart.Apply(GameFolder, Log))
                    {
                        Log?.Invoke($"    * FAIL");
                        return false;
                    }
                    Log?.Invoke($"    * OK");

                    installedPartVer.InstalledVersion = targetPart.VersionTo;
                    shouldContinue = true;
                }
            }

            GameInfo.Save();

            return true;
        }
    }
}
