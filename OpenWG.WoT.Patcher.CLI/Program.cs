﻿using System;
using System.IO;

namespace OpenWG.WoT.Patcher.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1 || args.Length > 2)
            {
                Console.WriteLine($"* {System.AppDomain.CurrentDomain.FriendlyName} <wot_dir>");
                Console.WriteLine($"* {System.AppDomain.CurrentDomain.FriendlyName} <wot_dir> <update_dir>");
                return;
            }

            var dir_wot = args[0];
            if (!Directory.Exists(dir_wot)) {
                Console.WriteLine($"   * directory {dir_wot} does not exists or is not a directory");
                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey();
                return;
            }

            var dir_update = Path.Join(dir_wot, "updates/");
            if(args.Length == 2)
            {
                dir_update = args[1];
            }
            if (!Directory.Exists(dir_update))
            {
                Console.WriteLine($"   * directory {dir_update} does not exists or is not a directory");
                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey();
                return;
            }

            Console.WriteLine($" Client Directory: {dir_wot}");
            Console.WriteLine($" Update Directory: {dir_update}");
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            var patcher = new Patcher(dir_wot, dir_update);
            if (!patcher.IsValid)
            {
                Console.WriteLine("invalid game directory or no updates in update directory");
                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey();
                return;
            }
            patcher.Log += Console.WriteLine;

            var result = patcher.Patch();

            Console.WriteLine("----------");

            if (result)
            {
                Console.WriteLine("Result: OK");
            }
            else
            {
                Console.WriteLine("Result: FAIL");
                Environment.ExitCode = 1;
            }

            Console.WriteLine("Press any key to exit ...");
            Console.ReadKey();
            return;
        }
    }
}
